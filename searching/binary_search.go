package searching

func BinarySearch(array []int , number int) bool {
	var high,low int = len(array)-1,0
	for high >= low{
		median := (low + high) / 2

		if array[median] == number{
			return true
		}else if array[median] < number {
			low = median + 1
		}else{
			high = median - 1
		}
	}
	return false
}
